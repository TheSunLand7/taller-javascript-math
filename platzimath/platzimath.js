class PlatziMath {
    /**Promedios */
    static avgAri(array) {
        let avg = (array.reduce((acc, elem) => acc + elem, 0)) / array.length;
        return avg;
    }
    static mediaGeo(array) {
        let mg = array.reduce((acc, elem) => acc * elem, 1) ** (1 / array.length);
        return mg;
    }
    static mediaH(array) {
        if (array.includes(0)) {
            return 'Number must be different from zero.'
        } else {
            let mh = array.length / array.reduce((acc, elem) => acc + (1 / elem), 0);
            return mh;
        }
    }
    /**Mediana */
    static mediana(array) {
        array.sort((a, b) => a - b);
        if (array.length % 2 !== 0) {
            return array[Math.floor(array.length / 2)];
        } else {
            let mid = array.length / 2;
            return this.avgAri([array[mid - 1], array[mid]]);
        }
    }

    /**Moda */
    static moda(array) {
        //Construye el histograma en un objeto
        let obj = array.reduce((acc, elem) => { !acc[elem] ? acc[elem] = 1 : acc[elem]++; return acc; }, {});
        //Obtiene los valores en un array y ordena de menor a mayor
        let orderedArray = Object.entries(obj).sort((a, b) => a[1] - b[1]);
        let moda_ = orderedArray[orderedArray.length - 1][0]; //Retorna el último elemento del array
        return moda_;
    }
}
