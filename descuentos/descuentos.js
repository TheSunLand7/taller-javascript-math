/******************* PRECIOS Y DESCUENTOS***********************/
const app = document.getElementById('app');
const br = document.createElement('br');

/* Creating a label and an input. */
const label1 = document.createElement('label');
label1.innerHTML = '<b>Price:</b> ';
const input1 = document.createElement('input');
input1.setAttribute('type', 'number');
input1.setAttribute('placeholder', 'Insert the price');
input1.setAttribute('required', "");

/* Creating a label and an input. */
const label2 = document.createElement('label');
label2.innerHTML = '<b>Promotion code:</b> ';
const input2 = document.createElement('input');
input2.setAttribute('type', 'text');
input2.setAttribute('placeholder', 'Insert code');

const btn = document.createElement('button');
btn.setAttribute('type', 'button');
btn.innerText = 'Get Discount';

const p = document.createElement('p');

let promotion = [
    {
        name: 'Platzi Vacation',
        discount: 15
    },
    {
        name: 'Platzi Weekend',
        discount: 30
    },
    {
        name: 'Platzi New Year',
        discount: 45
    },
];

// const arr = promotion.filter(item => item.name === 'Platzi New Year').map(item => item.discount);
// console.log(arr);
// let promotion = {
//     'Platzi Vacation': 15,
//     'Platzi Weekend': 30,
//     'Platzi New Year': 45,
// };

/**Promotion code */
btn.addEventListener('click', () => {
    if (!input1.value || !input2.value) {
        p.innerText = 'Please, insert values for both price and promotion code!';
    } else {
        //Solo con objetos
        // if (Object.keys(promotion).includes(input2.value)) {
        //     let discount = (+input1.value * (100 - promotion[input2.value])) / 100;
        //     p.innerText = `You get a ${promotion[input2.value]}% off. Price with discount: $${discount}`;
        // } else { p.innerText = 'There is no promotion with this code.'; }
        //Con array de objetos.
        if (promotion.flatMap(item => item.name).includes(input2.value)) {
            let findDiscount = promotion.find(item => item.name === input2.value);
            let discount = (+input1.value * (100 - findDiscount.discount)) / 100;
            p.innerText = `You get a ${findDiscount.discount}% off. Price with discount: $${discount}`;
        } else { p.innerText = 'There is no promotion with this code.'; }
    }
});

/**Percentage discount */
// btn.addEventListener('click', () => {
//     if (!input1.value || !input2.value) {
//         p.innerText = 'Please, insert values for price and discount!';
//     } else {
//         if (+input2.value < 100) {
//             let discount = (+input1.value * (100 - +input2.value)) / 100;
//             p.innerHTML = `<b style="color:blue">Total payment:</b> $${discount}`;
//         } else { p.innerHTML = `<i style="color:red">Discount higher than or equal to 100%</i>`; }
//     }
// });

app.appendChild(label1);
label1.appendChild(input1);

app.appendChild(label2);
label2.appendChild(input2);

app.appendChild(btn);
app.appendChild(br);

app.appendChild(p);