/* Análisis de Juanita*/
const medianaPersona = persona => {
    const salarios_ = salarios.find(item => item.name === persona).trabajos.map(item => item.salario);
    const medianaSalario = PlatziMath.mediana(salarios_);
    return medianaSalario;

}

const salaryProjection = persona => {
    //Array de los porcentajes de incremento
    let incrementPercentage = [];

    const salarios_ = salarios
        .find(item => item.name === persona).trabajos
        .map(item => item.salario);

    //Sacando el procentaje de incremento de salarios
    const increment = salarios_.forEach((item, index, arr) => {
        if (index === (arr.length - 1)) {
            return;
        }
        //No lo he multiplicado por 100 porque después tendré que dividirlo por 10 de nuevo.
        let increment = ((arr[index + 1] - arr[index]) / arr[index]);
        incrementPercentage.push(increment + 1);
    });

    //Aplicando la media geometrica de los incrementos a 2 decimales.
    const mg = PlatziMath.mediaGeo(incrementPercentage).toFixed(2);

    //Obteniendo el porcentaje. Es decir: 1.45 => 45% fue el incremento.
    const percent = (mg - 1).toFixed(2);

    //Calculamos su proyeccion salarial con el porcentaje obtenido.
    const projSalarial = salarios_[salarios_.length - 1] * (1 + +percent);
    return projSalarial;
}

/**Análisis empresarial */
const company = () => {
    const empresas = {};

    salarios
        .forEach(item => item.trabajos.forEach(item => {
            if (!empresas[item.empresa]) empresas[item.empresa] = {};
            else {
                if (!empresas[item.empresa][item.year]) empresas[item.empresa][item.year] = [];
                else empresas[item.empresa][item.year].push(item.salario);
            }
        }));
    return empresas;
}

function getMediana(nameCompany, year) {
    let salarios;
    if (!nameCompany) {
        console.warn('La empresa no existe en la base de datos');
    } else if (!year) {
        console.warn('La empresa aun no tiene registros en ese año');
    } else {
        for (const key in company()) {
            if (key === nameCompany) {
                salarios = company()[key][year];
            }
        }
    }
    return PlatziMath.mediana(salarios);

}

//Análisis General
function medianaPorPersona(name) {
    const salarios_ = salarios.find(item => item.name === name).trabajos.map(item => item.salario);
    const mediana_ = PlatziMath.mediana(salarios_);
    return mediana_;
}
function medianaGeneral() {
    const names = salarios.map(item => medianaPorPersona(item.name));
    return PlatziMath.mediana(names);
}
function medianaTop10() {
    const medianaSalarios = salarios.map(item => medianaPorPersona(item.name));
    medianaSalarios.sort((a, b) => a - b);
    const cantidad = medianaSalarios.length / 10;
    let limite = medianaSalarios.length - cantidad;
    const topSalarios = medianaSalarios.slice(limite, medianaSalarios.length);

    return PlatziMath.mediana(topSalarios);
}
console.log(medianaTop10());