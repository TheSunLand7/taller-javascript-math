// class Square {
//     constructor(_side) {
//         this.side = _side;
//     }
//     perimeter() { return this.side * 4; }
//     area() { return Math.pow(this.side, 2); }
// }

// class Triangle {
//     constructor({ side1, side2, base }) {
//         this.side1 = side1;
//         this.side2 = side2;
//         this.base = base;
//     }
//     perimeter() { return this.side1 + this.side2 + this.base; }
//     area(heigth) {
//         //Si el usuario no ingresa una altura.
//         let semiP = this.perimeter() / 2;
//         let op = semiP * (semiP - this.side1) * (semiP - this.side2) * (semiP - this.base);
//         return !heigth ? Math.sqrt(op) : (heigth * this.base) / 2;
//     }
// }

// class Circle {
//     constructor(_radio) {
//         this.radio = _radio;
//     }
//     perimeter() { return this.radio * 2 * Math.PI; }
//     area() { return Math.pow(this.radio, 2) * Math.PI; }
// }

// console.group('Square');
// const square = new Square(5);
// console.log(`Square perimeter: ${square.perimeter()}`);
// console.log(`Square area: ${square.area()}`);
// console.groupEnd();

// console.group('Triangle');
// const triangle = new Triangle({ side1: 13, side2: 10, base: 5 });
// console.log(`Triangle perimeter: ${triangle.perimeter()}`);
// console.log(`Triangle area: ${triangle.area(10)}`);
// console.groupEnd();

// console.group('Circle');
// const circle = new Circle(4);
// console.log(`Circle perimeter: ${circle.perimeter()}`);
// console.log(`Circle area: ${circle.area()}`);
// console.groupEnd();

// //Calculando la altura de un triangulo isosceles y escaleno.
// function heigthTriangleIso(side, base) {
//     if (side === base) {
//         console.warn("¡Este no es un triangulo isosceles!");
//     } else {
//         let op = Math.pow(side, 2) - (Math.pow(base, 2) / 4);
//         return Math.sqrt(op);
//     }
// }

// function heigthTriangleEsc({ side1, side2, base }) {
//     if (side1 === side2 || side1 === base || side2 === base) {
//         console.warn("¡Este no es un triángulo escaleno!");
//     } else {
//         let semiP = (side1 + side2 + base) / 2;
//         let op = semiP * (semiP - side1) * (semiP - side2) * (semiP - base);
//         return (2 * Math.sqrt(op)) / base;
//     }

// }
// console.log(`Isosceles triangle heigth: ${heigthTriangleIso(10, 12)}`);
// console.log(`Altura de un triángulo escaleno: ${heigthTriangleEsc({ side1: 12, side2: 10, base: 8 })}`);

